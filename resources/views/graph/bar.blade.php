@php

    $labelXTxt = json_encode($labelX);
  $dataSetTxt = json_encode($dataSet);
  $id = random_int(10000000,99999999);
@endphp

<div class="card-db">
    <div class="card-header">
        <i class="far fa-chart-bar" style="padding-right: 10px; padding-top: 5px"></i> {{$title}}
    </div>
    <div class="card-body"  >
        <div style="height: 300px">
            <canvas id="myChart{{$id}}"></canvas>
        </div>


        <div class="table-responsive">
            <table class="table" id="table{{$id}}">
                <thead>
                <tr>
                    <th>

                    </th>
                    @foreach( $labelX as  $label)
                        <th>
                            {{$label}}
                        </th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach( $dataSet as  $data)
                    <tr>
                        <td>{{$data['label']}}</td>
                        @foreach( $data['data'] as  $showdata)
                            <th>
                                {{$showdata}}
                            </th>
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('#table{{$id}}').DataTable(
            {
                "paging":   false,
                "ordering": false,
                "info":     false,
                "searching": false,
                "dom": 'lBfrtip',
                "buttons": [
                    'excel'
                ]
            }
        );
    } );


    var ctx = document.getElementById('myChart{{$id}}').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!! $labelXTxt !!},
            datasets: {!! $dataSetTxt !!}
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true, precision:0
                    },

                }]
            },
            responsive: true,
            maintainAspectRatio: false
        }
    });
</script>