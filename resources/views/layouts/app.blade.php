<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" crossorigin="anonymous">

    <link href="{{asset("css/all.css")}}" rel="stylesheet">
    <link href="{{asset("js/Chart/Chart.css")}}" rel="stylesheet">
    <link href="{{asset("js/select2/select2.min.css")}}" rel="stylesheet">
    <title>Dashboard - @yield('title')</title>

    <style>
        .health-nav{
            background: #006738;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
        }

        .select-box{
            padding: 10px;
        }

        .table{
            font-size: 14px;
        }

        .main-div{
            background: #FCFCFC;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-radius: 8px;
            margin-bottom: 10px;
            padding-bottom: 10px;

        }

        .card-db{
            border: 3px solid #006738;
            box-sizing: border-box;
            border-radius: 8px;
            margin-top: 10px;
        }

        .card-db .card-foot{
            background: #E1E1E1;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-bottom-left-radius:  8px;
            border-bottom-right-radius:  8px;

            margin: 0px;
            padding-left: 10px;
            font-size: 20px;
            height: 50px;
            padding-top: 10px
        }

        .card-db .card-header{
            background: #006738; color: white; font-size: 20px; padding-left: 10px; height: 45px; padding-top: 7px
        }

        .card-db .card-body{
            padding-left: 10px; padding-right: 10px; font-size: 30px
        }

        .dt-buttons{
            font-size: 14px;
            width: 50px;
            text-align: center;
        }
    </style>

    <script src="{{asset("js/Chart/Chart.js")}}"></script>
    <script src="{{asset("js/jquery-3.3.1.slim.min.js")}}"></script>
    <script src="{{asset("js/select2/select2.min.js")}}" defer></script>
     <script src="{{asset("js/jquery.dataTables.min.js")}}" ></script>
    <script src="{{asset("js/dataTables.bootstrap4.min.js")}}" ></script>
    <script src="{{asset("js/popper.min.js")}}" ></script>
    <script src="{{asset("js/bootstrap.min.js")}}" ></script>
    <script src="{{asset("js/palette.js")}}"></script>
    <script src="{{asset("js/dataTables.select.min.js")}}"></script>
    <script src="{{asset("js/dataTables.buttons.min.js")}}"></script>
    <script src="{{asset("js/buttons.flash.min.js")}}"></script>
    <script src="{{asset("js/jszip.min.js")}}"></script>
    <script src="{{asset("js/pdfmake.min.js")}}"></script>
    <script src="{{asset("js/vfs_fonts.js")}}"></script>
    <script src="{{asset("js/buttons.html5.min.js")}}"></script>
    <script src="{{asset("js/buttons.print.min.js")}}"></script>


    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script>
</head>
<body>



<nav class="health-nav navbar navbar-expand-md navbar-dark bg-dark sticky-top">
    <a href="{{url('')}}" class="navbar-brand"> ระบบรายงานสถานะการณ์ การจมน้ำ @yield('db')</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav  ml-auto">
            <a href="{{url('hdc')}}" class="nav-item nav-link">HDC</a>
            <a href="{{url('is')}}" class="nav-item nav-link">IS</a>
            <a href="{{url('items')}}" class="nav-item nav-link">ITEMS</a>
            <a href="{{url('deathcert')}}" class="nav-item nav-link">มรณะบัตร</a>
            <a href="{{url('sat')}}" class="nav-item nav-link">SAT</a>
        </div>
    </div>


</nav>
<div class="container">
    <div class="row" id="search-bar" style="min-height: 30px; margin-top: 30px; vertical-align: central;">
        <div class="select-box col-sm-12 col-md-3">
              <label style="width: 20%"> ปี  </label>
            <select  style="width: 75%" name="year" id="year">
                @for($year = 2015; $year <= date("Y"); $year++ )
                    <option @if( isset($_GET['year']) && $_GET['year'] == $year)  selected  @endif
                    value="{{$year}}">{{$year}}</option>
                @endfor
            </select>
        </div>
        <div class="select-box col-sm-12 col-md-4">
            <label style="width: 20%"> จังหวัด  </label>
            <select style="width: 75%" name="" id="province">
                @php
                    $provinces = \App\Province::select("LOC_CODE","LOC_PROVINCE")->get();
                @endphp
                @foreach($provinces as $province )
                    <option @if( isset($_GET['province']) && $_GET['province'] == $province->LOC_CODE)  selected  @endif
                            value="{{$province->LOC_CODE}}">{{$province->LOC_PROVINCE}}</option>
                @endforeach
            </select>
        </div>

        <button id="search-btn" class="btn btn-secondary col-sm-12 col-md-3" >
            ค้นหา
        </button>
    </div>
        <hr>
</div>
<script>

    $(document).ready(function() {
        $('#province').select2();

        if(window.location.pathname == '/drowningdashboard/public/'){
            $('#search-bar').hide();
        }ร
    });




    $("#search-btn").click(function () {
        var province = $('#province').val();
        var year = $('#year').val();
        var url = window.location.protocol + '//' + window.location.hostname + window.location.pathname   + "?province=" + province + "&year=" + year;
        window.location = url;
    });

</script>


<div class="container">


    @yield('content')
</div>

</body>
</html>
