
@extends('layouts.app')

@section('title', 'Home')
@section('db', '> ข้อมูลมรณะบัตร')

@section('content')

    <div class="container main-div">

        <div class="row ">
            @foreach($graphList as $graph)
                <div class="col-sm-12 col-md-12 col-lg-6">
                    @include('graph.'.$graph['graph'], $graph)
                </div>
            @endforeach

        </div>
    </div>
@endsection