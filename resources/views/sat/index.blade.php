
@extends('layouts.app')

@section('title', 'Home')
@section('db', '> SAT')

@section('content')

    <div class="container main-div">

        <div class="row ">
            <div class="col-sm-12 col-md-12 col-lg-6">
                @include('graph.bar', [])
            </div>

            <div class="col-sm-12 col-md-12 col-lg-6">
                @include('graph.line', [])
            </div>


            <div class="col-sm-12 col-md-12 col-lg-6">
                @include('graph.pie', [])
            </div>

        </div>


    </div>
@endsection