
@extends('layouts.app')

@section('title', 'Home')
@section('db', '')

@section('content')
    <style>
        .main-div{
            background: #FCFCFC;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-radius: 8px;
            margin-bottom: 10px;
            padding-bottom: 10px;

        }

        .card-db{
            border: 3px solid #006738;
            box-sizing: border-box;
            border-radius: 8px;
            margin-top: 10px;
        }

        .card-db .card-foot{
            background: #E1E1E1;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-bottom-left-radius:  8px;
            border-bottom-right-radius:  8px;

            margin: 0px;
            padding-left: 10px;
            font-size: 20px;
            height: 50px;
            padding-top: 10px
        }

        .card-db .card-header{
            background: #006738; color: white; font-size: 20px; padding-left: 10px; height: 45px; padding-top: 7px
        }

        .card-db .card-body{
            padding-left: 10px; padding-right: 10px; font-size: 30px
        }
    </style>
    <div class="container main-div">
        <div class="row ">

            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card-db">
                    <div class="card-header">
                        <i class="far fa-chart-bar" style="padding-right: 10px; padding-top: 5px"></i>  DeathCert
                    </div>
                    <div class="card-body" style="">
                        เสียชีวิต: {{$death_row}} ราย  <br>
                    </div>
                    <a href="{{url('deathcert')}}">
                    <div class="row card-foot " style="">

                            <div class="col-7" style="padding-left: 5px">
                                ดูเพิ่มเติม
                            </div>
                            <div class="col">
                                <i class="far fa-caret-square-right float-right" style=" padding-top: 5px"></i>
                            </div>
                    </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card-db">
                    <div class="card-header">
                        <i class="far fa-chart-bar" style="padding-right: 10px; padding-top: 5px"></i>  IS
                    </div>
                    <div class="card-body" style="">
                        เสียชีวิต: {{$is_death_row}} ราย  <br>
                        ไม่เสียชีวิต: {{$is_live_row}}  ราย
                    </div>
                    <a href="{{url('is')}}">
                        <div class="row card-foot " style="">

                            <div class="col-7" style="padding-left: 5px">
                                ดูเพิ่มเติม
                            </div>
                            <div class="col">
                                <i class="far fa-caret-square-right float-right" style=" padding-top: 5px"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card-db">
                    <div class="card-header">
                        <i class="far fa-chart-bar" style="padding-right: 10px; padding-top: 5px"></i>  HDC
                    </div>
                    <div class="card-body" style="">
                        Death: {{$hdc_death}} ราย  <br>
                        Accident: {{$hdc_accident}}  ราย <br>
                        OPD: {{$hdc_opd}}  ราย <br>
                        IPD: {{$hdc_ipd}}  ราย <br>
                    </div>
                    <a href="{{url('hdc')}}">
                        <div class="row card-foot " style="">

                            <div class="col-7" style="padding-left: 5px">
                                ดูเพิ่มเติม
                            </div>
                            <div class="col">
                                <i class="far fa-caret-square-right float-right" style=" padding-top: 5px"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card-db">
                    <div class="card-header">
                        <i class="far fa-chart-bar" style="padding-right: 10px; padding-top: 5px"></i>  ITEMS
                    </div>
                    <div class="card-body" style="">
                        จำนวน: {{$items_row}} ครั้ง
                    </div>
                    <a href="{{url('items')}}">
                        <div class="row card-foot " style="">

                            <div class="col-7" style="padding-left: 5px">
                                ดูเพิ่มเติม
                            </div>
                            <div class="col">
                                <i class="far fa-caret-square-right float-right" style=" padding-top: 5px"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="card-db">
                    <div class="card-header">
                        <i class="far fa-chart-bar" style="padding-right: 10px; padding-top: 5px"></i>  SAT
                    </div>
                    <div class="card-body" style="">
                        เสียชีวิต: 2 ราย  <br>
                        ไม่เสียชีวิต: 1  ราย
                    </div>
                    <a href="{{url('sat')}}">
                        <div class="row card-foot " style="">

                            <div class="col-7" style="padding-left: 5px">
                                ดูเพิ่มเติม
                            </div>
                            <div class="col">
                                <i class="far fa-caret-square-right float-right" style=" padding-top: 5px"></i>
                            </div>
                        </div>
                    </a>
                </div>
            </div>


        </div>
    </div>
@endsection