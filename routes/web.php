<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('','DashboardController');
Route::get('home','DashboardController');
Route::get('is','ISController@index');
Route::get('sat','DashboardController@satIndex');
Route::get('hdc','HDCController@index');
Route::get('items','ITEMSController@index');
Route::get('deathcert','DeathCertController@index');