<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HDCdata extends Model
{
    //
    protected $table = "persons_death";
    protected $dates = ['dateadmit'];


    function deathData(){



    }

    public static function personDeathRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'count'
                    FROM persons_death"));

        return $results[0];
    }

    public static function personAccidentRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'count'
                    FROM persons_accident"));

        return $results[0];
    }

    public static function personOPDRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'count'
                    FROM persons_OPD"));

        return $results[0];
    }

    public static function personIPDRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'count'
                    FROM persons_IPD"));

        return $results[0];
    }


}
