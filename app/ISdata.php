<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ISdata extends Model
{
    //
    protected $table = "is_drowning";
    protected $dates = ['adate'];


    function deathData(){



    }

    public static function deathRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'count'
                    FROM is_drowning
                    WHERE ( staer = '1' or staer = '6' or staward = '5' )"));

        return $results[0];
    }

    public static function liveRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'count'
                    FROM is_drowning
                    WHERE ( staer != '1' and staer != '6' and staward = '5' )"));

        return $results[0];
    }
}
