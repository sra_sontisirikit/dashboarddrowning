<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeathCert extends Model
{
    //
    protected $table = "death_certs";
    protected $dates = ['date_dead'];
}
