<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ITEMSdata extends Model
{
    //
    protected $table = "items_dis";


    public static function caseRow(){
        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'count'
                    FROM items_dis"));

        return $results[0];
    }
}
