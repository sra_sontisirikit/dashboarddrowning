<?php

namespace App\Http\Controllers;

use App\DeathCert;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HDCController extends GraphController
{

    public $opdCase_Month_Year;
    public $ipdCase_Month_Year;
    public $deathCase_Month_Year;
    public $accidentCase_Month_Year;

    public $year;
    public $province;
    public $colorsYear;

    public $table = "is_drowning";
    public $districts = "";

    public function index(Request $request){

        $this->province = 10;
        if($request->input("province")){
            $this->province = $request->input("province");
        }
        $this->year = 2018;
        if($request->input("year")){
            $this->year = $request->input("year");
        }

        $this->districts = $this->getDistricts();
        $this->colorsYear();
        $this->colorsIndex();

        $this->opdCase_Month_Year = $this->dataCaseMonthYear("persons_opd");
        $this->ipdCase_Month_Year = $this->dataCaseMonthYear("persons_ipd");
        $this->accidentCase_Month_Year = $this->dataCaseMonthYear("persons_accident");
        $this->deathCase_Month_Year = $this->dataCaseMonthYear("persons_death");

        $this->caseYear($this->opdCase_Month_Year, "ผู้บาดเจ็บจากแฟ้ม Diagnosis_opd");
        $this->caseYear($this->ipdCase_Month_Year, "ผู้บาดเจ็บจากแฟ้ม Diagnosis_ipd");
        $this->caseYear($this->accidentCase_Month_Year, "ผู้เสียชีวิตจากแฟ้ม Death");
        $this->caseYear($this->deathCase_Month_Year, "ผู้บาดเจ็บจากแฟ้ม Accident");


        return view('hdc.index', $this->dataGraph);
    }

    public function dataCaseMonthYear($table){


        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'x',  MONTH(dateadmit) as 'month', YEAR(dateadmit) as 'year' 
                    FROM $table, lib_hospcode
                     WHERE $table.hospcode = lib_hospcode.off_id
                    and lib_hospcode.changwatcode = $this->province
                    and YEAR(dateadmit) >= 2015
                    GROUP BY  MONTH(dateadmit) , YEAR(dateadmit) 
                     ORDER BY MONTH(dateadmit) ASC, YEAR(dateadmit) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function caseYear($results, $title){

        $labelX = [1,2,3,4,5,6,7,8,9,10,11,12];
//        $labelX = $results->unique('month')->pluck('month');
        $labelYTxt = $results->unique('year')->pluck('year')->sort();

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }
            foreach ($data as $x_data){
                $data_val[$x_data->month] = $x_data->x;
            }


            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['fill'] = false;
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'line';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;

    }



}
