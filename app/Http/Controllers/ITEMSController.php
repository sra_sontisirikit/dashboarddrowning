<?php

namespace App\Http\Controllers;

use App\DeathCert;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ITEMSController extends GraphController
{

    public $case_Month_Year;
    public $case_Red_Month_Year;
    public $case_Yellow_Month_Year;
    public $case_Green_Month_Year;

    public $case_District_Month_Year;
    public $case_SubDistrict_Month_Year;


    public $year;
    public $province;
    public $colorsYear;

    public $table = "items_dis";
    public $districts = "";

    public function index(Request $request){

        $this->province = 10;
        if($request->input("province")){
            $this->province = $request->input("province");
        }
        $this->year = 2018;
        if($request->input("year")){
            $this->year = $request->input("year");
        }

//        $this->districts = $this->getDistricts();
       $this->colorsYear();
//        $this->colorsIndex();
        $this->case_Month_Year = $this->dataCaseMonthYear("items_dis");
        $this->case_Red_Month_Year = $this->dataCaseColorMonthYear("items_dis","แดง");
        $this->case_Yellow_Month_Year = $this->dataCaseColorMonthYear("items_dis","เหลือง");
        $this->case_Green_Month_Year = $this->dataCaseColorMonthYear("items_dis", "เขียว");
        $this->case_District_Month_Year = $this->dataCaseDistrictYear("items_dis");
        $this->case_SubDistrict_Month_Year = $this->dataCaseSubDistrictYear("items_dis");
        $this->caseYear($this->case_Month_Year, "ผู้บาดเจ็บจากการจมน้ำ");
        $this->caseYear($this->case_Red_Month_Year, "ผู้บาดเจ็บจากการจมน้ำ(แดง)");
        $this->caseYear($this->case_Yellow_Month_Year, "ผู้บาดเจ็บจากการจมน้ำ(เหลือง)");
        $this->caseYear($this->case_Green_Month_Year, "ผู้บาดเจ็บจากการจมน้ำ(เขียว)");
        $this->caseAreaYear($this->case_District_Month_Year, "ผู้บาดเจ็บจากการจมน้ำตามอำเภอ");
        $this->caseAreaYear($this->case_SubDistrict_Month_Year, "ผู้บาดเจ็บจากการจมน้ำตามตำบล");

        return view('items.index', $this->dataGraph);
    }

    public function dataCaseMonthYear($table){


        $sql = "SELECT COUNT(*) as 'x',
                    MONTH(A2_1_1) as 'month',
                    YEAR(A2_1_1) as 'year' 
                    FROM ".$table."
                    WHERE  ".$table.".ProvinceId = $this->province
                    and YEAR(A2_1_1) >= 2015
                    GROUP BY  MONTH(A2_1_1) , YEAR(A2_1_1) 
                    ORDER BY MONTH(A2_1_1) ASC, YEAR(A2_1_1) ASC";

        $results = DB::select( DB::raw($sql));

        $results =  collect( $results );

        return $results;
    }

    public function dataCaseColorMonthYear($table,$color){


        $sql = "SELECT COUNT(*) as 'x',
                    MONTH(A2_1_1) as 'month',
                    YEAR(A2_1_1) as 'year' 
                    FROM $table
                    WHERE  $table.ProvinceId = $this->province
                    and  $table.InjuryColor = '$color'
                    and YEAR(A2_1_1) >= 2015
                    GROUP BY  MONTH(A2_1_1) , YEAR(A2_1_1) 
                    ORDER BY MONTH(A2_1_1) ASC, YEAR(A2_1_1) ASC";

        $results = DB::select( DB::raw($sql));

        $results =  collect( $results );

        return $results;
    }

    public function dataCaseDistrictYear($table){

        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'x', $table.AmphurName as 'name' , YEAR(A2_1_1) as 'year' 
                    FROM $table WHERE ProvinceId =  $this->province
                    and $table.AmphurName is not null 
                    and YEAR(A2_1_1) >= 2015
                    GROUP BY $table.AmphurName , YEAR(A2_1_1) 
                    ORDER BY YEAR(A2_1_1) ASC, MONTH(A2_1_1) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function dataCaseSubDistrictYear($table){

        $results = DB::select( DB::raw(
            "SELECT COUNT(*) as 'x', Concat($table.AmphurName,' ',$table.TambolName) as 'name' , YEAR(A2_1_1) as 'year' 
                    FROM $table WHERE ProvinceId =  $this->province
                    and $table.AmphurName is not null 
                    and YEAR(A2_1_1) >= 2015
                    GROUP BY $table.AmphurName, $table.TambolName , YEAR(A2_1_1) 
                    ORDER BY YEAR(A2_1_1) ASC, MONTH(A2_1_1) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function caseYear($results, $title){



        $labelX = [1,2,3,4,5,6,7,8,9,10,11,12];
//        $labelX = $results->unique('month')->pluck('month');
        $labelYTxt = $results->unique('year')->pluck('year')->sort();

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }
            foreach ($data as $x_data){
                $data_val[$x_data->month] = $x_data->x;
            }


            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['fill'] = false;
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'line';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;

    }

    public function caseAreaYear($results, $title){


        $labelX = $results->unique('name')->pluck('name');
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }

            foreach ($data as $x_data){
                $data_val[$x_data->name] = $x_data->x;
            }

            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['fill'] = true;
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }



}
