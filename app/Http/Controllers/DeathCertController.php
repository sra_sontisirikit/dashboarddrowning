<?php

namespace App\Http\Controllers;

use App\DeathCert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DeathCertController extends GraphController
{

    public $deadCase_Month_Year;
    public $deadCase_District_Year;
    public $deadCase_AgeRange_AtYear;
    public $deadCase_AgeRange_Year;
    public $deadCase_Gender_Year;
    public $year;
    public $province;
    public $colorsYear;

    public function index(Request $request){

        $this->province = 10;
        if($request->input("province")){
            $this->province = $request->input("province");
        }
        $this->year = 2018;
        if($request->input("year")){
            $this->year = $request->input("year");
        }

        $this->colors();
        $this->colorsIndex();

        $this->deadCase_Month_Year = $this->deadCaseMonthYear();
        $this->deadCase_District_Year = $this->deadCaseDistrictYear();
        $this->deadCase_AgeRange_AtYear = $this->deadCaseAgeRangeAtYear();
        $this->deadCase_AgeRange_Year = $this->deadCaseAgeRangeYear();
        $this->deadCase_Gender_Year = $this->deadCaseGenderYear();
        $this->deathFourYear();
        $this->deathDistrictYear();
        $this->deathAgeRangeAtYear();
        $this->deathAgeRangeYear();
        $this->deathGenderYear();


        return view('deathcert.index', $this->dataGraph);
    }

    public function colors(){

        $this->colorsYear = [];
        for ($year = 2000; $year < 2050; $year++){
            $this->colorsYear[$year] = ranColor();
        }

    }

    public function deadCaseMonthYear(){

        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'x',  MONTH(date_dead) as 'month', YEAR(date_dead) as 'year' 
                    FROM death_certs WHERE province_code = $this->province
                    GROUP BY  MONTH(date_dead) , YEAR(date_dead) 
                     ORDER BY YEAR(date_dead) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function deadCaseDistrictYear(){

        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'x', district , YEAR(date_dead) as 'year' 
                    FROM death_certs WHERE province_code =  $this->province
                    and district_code is not null 
                    GROUP BY district , YEAR(date_dead) 
                    ORDER BY YEAR(date_dead) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function deadCaseAgeRangeAtYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(age <= 14,1,0)) as '0 - 14',
                    SUM(IF(age BETWEEN 15 and 24,1,0)) as '15 - 24',
                    SUM(IF(age BETWEEN 25 and 34,1,0)) as '25 - 34',
                    SUM(IF(age BETWEEN 35 and 44,1,0)) as '35 - 44',
                    SUM(IF(age BETWEEN 45 and 54,1,0)) as '45 - 54',
                    SUM(IF(age BETWEEN 55 and 64,1,0)) as '55 - 64',
                    SUM(IF(age BETWEEN 65 and 74,1,0)) as '65 - 74',
                    SUM(IF(age > 75,1,0)) as '>= 75'
                    FROM death_certs 
                    WHERE province_code = $this->province 
                    and year_dead = $this->year "));

        $results =  collect( $results );

        return $results;
    }

    public function deadCaseAgeRangeYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(age <= 14,1,0)) as '0 - 14',
                    SUM(IF(age BETWEEN 15 and 24,1,0)) as '15 - 24',
                    SUM(IF(age BETWEEN 25 and 34,1,0)) as '25 - 34',
                    SUM(IF(age BETWEEN 35 and 44,1,0)) as '35 - 44',
                    SUM(IF(age BETWEEN 45 and 54,1,0)) as '45 - 54',
                    SUM(IF(age BETWEEN 55 and 64,1,0)) as '55 - 64',
                    SUM(IF(age BETWEEN 65 and 74,1,0)) as '65 - 74',
                    SUM(IF(age > 75,1,0)) as '>= 75',
                    year_dead as 'year'
                    FROM death_certs 
                    WHERE province_code = $this->province 
                    GROUP BY year_dead "));

        $results =  collect( $results );

        return $results;
    }

    public function deadCaseGenderYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(gender = 'ชาย',1,0)) as 'ชาย',
                    SUM(IF(gender = 'หญิง',1,0)) as 'หญิง',
                    year_dead as 'year'
                    FROM death_certs 
                    WHERE province_code = $this->province 
                    GROUP BY year_dead "));

        $results =  collect( $results );

        return $results;
    }


    public function deathFourYear(){


        $results = $this->deadCase_Month_Year;

        $labelX = $results->unique('month')->pluck('month');
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }
            foreach ($data as $x_data){
                $data_val[$x_data->month] = $x_data->x;
            }


            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['fill'] = false;
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = "จำนวนตายรายเดือน";
        $set['graph'] = 'line';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;

    }

    public function deathDistrictYear(){

        $results = $this->deadCase_District_Year;

        $labelX = $results->unique('district')->pluck('district');
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }
            foreach ($data as $x_data){
                $data_val[$x_data->district] = $x_data->x;
            }


            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['fill'] = true;
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = "จำนวนตายตามอำเภอ";
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }

    public function deathAgeRangeAtYear(){

        $results = $this->deadCase_AgeRange_AtYear;
        $results = $results->first();
        $attributes = array_keys(get_object_vars ( $results) );
        $vals = array_values(get_object_vars ( $results) );
        $labelX = $attributes;
        $labelYTxt = "เสียชีวิต";

        $dataSet = [];
        $colors = ranColors(count( $labelX));
            $arr = array();
            $arr['label'] = $labelYTxt;
            $arr['borderColor'] = $colors;
            $arr['fill'] = true;
            $arr['backgroundColor'] = $colors;
            $arr['data'] = $vals;
            $dataSet[] = $arr;


        $set = [];
        $set['title'] = "จำนวนตายตามช่วงอายุ ปี ".$this->year;
        $set['graph'] = 'pie';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;


    }


    public function deathAgeRangeYear(){


        $results = $this->deadCase_AgeRange_AtYear;
        $results = $results->first();
        $attributes = array_keys(get_object_vars ( $results) );

        $results = $this->deadCase_AgeRange_Year;
        $labelX = $attributes;
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            try{
                $data = (array) $results->where('year', '===', $year)->first();
                $data_val = [];
                foreach ($labelX as $x){
                    $data_val[$x] = 0;
                }
                unset($data['year']);
                $data_val = $data;

                $arr = array();
                $arr['label'] = $year;
                $arr['borderColor'] =  $this->colorsYear[$year];
                $arr['fill'] = true;
                $arr['backgroundColor'] =  $this->colorsYear[$year];
                $arr['data'] = array_values($data_val);
                $dataSet[] = $arr;
            }catch ( \Exception $e){
                dd($results->where('year', '===', $year));
            }
        }

        $set = [];
        $set['title'] = "จำนวนตายตามช่วงอายุ";
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }

    public function deathGenderYear(){


        $results = $this->deadCase_Gender_Year;
        $results = $results->first();
        $attributes = array_keys(get_object_vars ( $results) );
        unset($attributes["2"]);

        $results = $this->deadCase_Gender_Year;
        $labelX = $attributes;
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            try{
                $data = (array) $results->where('year', '===', $year)->first();
                $data_val = [];
                foreach ($labelX as $x){
                    $data_val[$x] = 0;
                }
                unset($data['year']);
                $data_val = $data;

                $arr = array();
                $arr['label'] = $year;
                $arr['borderColor'] =  $this->colorsYear[$year];
                $arr['fill'] = true;
                $arr['backgroundColor'] =  $this->colorsYear[$year];
                $arr['data'] = array_values($data_val);
                $dataSet[] = $arr;
            }catch ( \Exception $e){
                dd($results->where('year', '===', $year));
            }
        }

        $set = [];
        $set['title'] = "จำนวนตายตามเพศ";
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }


}
