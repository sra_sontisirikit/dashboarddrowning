<?php

namespace App\Http\Controllers;

use App\DeathCert;
use App\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ISController extends GraphController
{

    public $deadCase_Month_Year;
    public $liveCase_Month_Year;

    public $deadCase_District_Year;
    public $liveCase_District_Year;

    public $deadCase_AgeRange_AtYear;
    public $liveCase_AgeRange_AtYear;

    public $deadCase_AgeRange_Year;
    public $deadCase_Gender_Year;


    public $table = "is_drowning";
    public $districts = "";

    public function index(Request $request){

        $this->province = 10;
        if($request->input("province")){
            $this->province = $request->input("province");
        }
        $this->year = 2018;
        if($request->input("year")){
            $this->year = $request->input("year");
        }

        $this->districts = $this->getDistricts();
        $this->colorsYear();
        $this->colorsIndex();

        $this->deadCase_Month_Year = $this->deadCaseMonthYear();
        $this->liveCase_Month_Year = $this->liveCaseMonthYear();
        $this->deadCase_District_Year = $this->deadCaseDistrictYear();
        $this->liveCase_District_Year = $this->liveCaseDistrictYear();
        $this->deadCase_AgeRange_AtYear = $this->deadCaseAgeRangeAtYear();
        $this->liveCase_AgeRange_AtYear = $this->liveCaseAgeRangeAtYear();
        $this->deadCase_AgeRange_Year = $this->caseAgeRangeYear();
        $this->deadCase_Gender_Year = $this->caseGenderYear();
        $this->deathFourYear("จำนวนเสียชีวิตรายเดือน");
        $this->liveFourYear("จำนวนรอดชีวิตรายเดือน");
        $this->districtYear( $this->deadCase_District_Year , "จำนวนเสียชีวิตตามอำเภอ");
        $this->districtYear( $this->liveCase_District_Year , "จำนวนรอดชีวิตตามอำเภอ");
        $this->ageRangeAtYear(  $this->deadCase_AgeRange_AtYear, "จำนวนเสียชีวิตตามช่วงอายุ ปี".$this->year);
        $this->ageRangeAtYear(  $this->liveCase_AgeRange_AtYear, "จำนวนรอดชีวิตตามช่วงอายุ ปี".$this->year);
        $this->ageRangeYear("จำนวนผู้บาดเจ็บแลัเสียชีวิตตามช่วงอายุ");
        $this->deathGenderYear("จำนวนผู้บาดเจ็บแลัเสียชีวิตตามเพศ");


        return view('is.index', $this->dataGraph);
    }




    public function setDistricts($list){

        $districts_arr = [];
        foreach ($list as $district_id){
            if((int) $district_id > 0){
                $districts_arr[] = $this->districts[ (int) $district_id];
            }
        }
        return $districts_arr;
    }



    public function colorsIndex(){

        $this->colorsIndex = [];
        for ($i = 0; $i < 50; $i++){
            $this->colorsIndex[] = ranColor();
        }
    }

    public function deadCaseMonthYear(){

        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'x',  MONTH(adate) as 'month', YEAR(adate) as 'year' 
                    FROM ".$this->table." WHERE changwat = $this->province
                    and ( staer = '1' or staer = '6' or staward = '5' )
                    and YEAR(adate) >= 2015
                    GROUP BY  MONTH(adate) , YEAR(adate) 
                     ORDER BY MONTH(adate) ASC, YEAR(adate) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function liveCaseMonthYear(){

        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'x',  MONTH(adate) as 'month', YEAR(adate) as 'year' 
                    FROM ".$this->table." WHERE changwat = $this->province
                    and ( staer != '1' and staer != '6' and staward != '5' )
                    and YEAR(adate) >= 2015
                    GROUP BY  MONTH(adate) , YEAR(adate) 
                    ORDER BY MONTH(adate) ASC, YEAR(adate) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function deadCaseDistrictYear(){

        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'x', ampur , YEAR(adate) as 'year' 
                    FROM ".$this->table." WHERE changwat =  $this->province
                    and ( staer = '1' or staer = '6' or staward = '5' )
                    and ampur is not null 
                    and YEAR(adate) >= 2015
                    GROUP BY ampur , YEAR(adate) 
                    ORDER BY YEAR(adate) ASC, MONTH(adate) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function liveCaseDistrictYear(){

        $results = DB::select( DB::raw(
            "SELECT COUNT(id) as 'x', ampur , YEAR(adate) as 'year' 
                    FROM ".$this->table." WHERE changwat =  $this->province
                   and ( staer != '1' and staer != '6' and staward != '5' )
                    and ampur is not null 
                    and YEAR(adate) >= 2015
                    GROUP BY ampur , YEAR(adate) 
                    ORDER BY YEAR(adate) ASC, MONTH(adate) ASC
                    "));

        $results =  collect( $results );

        return $results;
    }

    public function deadCaseAgeRangeAtYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(age <= 14,1,0)) as '0 - 14',
                    SUM(IF(age BETWEEN 15 and 24,1,0)) as '15 - 24',
                    SUM(IF(age BETWEEN 25 and 34,1,0)) as '25 - 34',
                    SUM(IF(age BETWEEN 35 and 44,1,0)) as '35 - 44',
                    SUM(IF(age BETWEEN 45 and 54,1,0)) as '45 - 54',
                    SUM(IF(age BETWEEN 55 and 64,1,0)) as '55 - 64',
                    SUM(IF(age BETWEEN 65 and 74,1,0)) as '65 - 74',
                    SUM(IF(age > 75,1,0)) as '>= 75'
                    FROM ".$this->table." 
                    WHERE changwat = $this->province 
                    and ( staer = '1' or staer = '6' or staward = '5' )
                    and YEAR(adate) = $this->year "));

        $results =  collect( $results );

        return $results;
    }

    public function liveCaseAgeRangeAtYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(age <= 14,1,0)) as '0 - 14',
                    SUM(IF(age BETWEEN 15 and 24,1,0)) as '15 - 24',
                    SUM(IF(age BETWEEN 25 and 34,1,0)) as '25 - 34',
                    SUM(IF(age BETWEEN 35 and 44,1,0)) as '35 - 44',
                    SUM(IF(age BETWEEN 45 and 54,1,0)) as '45 - 54',
                    SUM(IF(age BETWEEN 55 and 64,1,0)) as '55 - 64',
                    SUM(IF(age BETWEEN 65 and 74,1,0)) as '65 - 74',
                    SUM(IF(age > 75,1,0)) as '>= 75'
                    FROM ".$this->table." 
                    WHERE changwat = $this->province 
                    and ( staer != '1' and staer != '6' and staward != '5' )
                    and YEAR(adate) = $this->year "));

        $results =  collect( $results );

        return $results;
    }

    public function caseAgeRangeYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(age <= 14,1,0)) as '0 - 14',
                    SUM(IF(age BETWEEN 15 and 24,1,0)) as '15 - 24',
                    SUM(IF(age BETWEEN 25 and 34,1,0)) as '25 - 34',
                    SUM(IF(age BETWEEN 35 and 44,1,0)) as '35 - 44',
                    SUM(IF(age BETWEEN 45 and 54,1,0)) as '45 - 54',
                    SUM(IF(age BETWEEN 55 and 64,1,0)) as '55 - 64',
                    SUM(IF(age BETWEEN 65 and 74,1,0)) as '65 - 74',
                    SUM(IF(age > 75,1,0)) as '>= 75',
                    YEAR(adate) as 'year'
                    FROM ".$this->table." 
                    WHERE changwat = $this->province 
                    GROUP BY YEAR(adate) "));

        $results =  collect( $results );

        return $results;
    }

    public function caseGenderYear(){

        $results = DB::select( DB::raw(
            "SELECT
                    SUM(IF(sex = 1,1,0)) as 'ชาย',
                    SUM(IF(sex = 2,1,0)) as 'หญิง',
                    YEAR(adate) as 'year'
                    FROM ".$this->table." 
                    WHERE changwat = $this->province 
                    GROUP BY YEAR(adate) "));

        $results =  collect( $results );

        return $results;
    }


    public function deathFourYear($title){

        $results = $this->deadCase_Month_Year;

        $labelX = [1,2,3,4,5,6,7,8,9,10,11,12];
//        $labelX = $results->unique('month')->pluck('month');
        $labelYTxt = $results->unique('year')->pluck('year')->sort();

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }
            foreach ($data as $x_data){
                $data_val[$x_data->month] = $x_data->x;
            }


            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['fill'] = false;
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'line';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;

    }

    public function liveFourYear($title){

        $results = $this->liveCase_Month_Year;

        $labelX = [1,2,3,4,5,6,7,8,9,10,11,12];
//        $labelX = $results->unique('month')->pluck('month');
        $labelYTxt = $results->unique('year')->pluck('year')->sort();

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }
            foreach ($data as $x_data){
                $data_val[$x_data->month] = $x_data->x;
            }

            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['fill'] = false;
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'line';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;

    }


    public function districtYear($results, $title){

        $labelX = $results->unique('ampur')->pluck('ampur');
        $labelX = $this->setDistricts($labelX);
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            $data = $results->where('year', '===', $year);
            $data_val = [];
            foreach ($labelX as $x){
                $data_val[$x] = 0;
            }


            foreach ($data as $x_data){
                if((int) $x_data->ampur > 0){
                    $ampur = $this->districts[(int) $x_data->ampur];
                    $data_val[$ampur] = $x_data->x;
                }
            }


            $arr = array();
            $arr['label'] = $year;
            $arr['borderColor'] =  $this->colorsYear[$year];
            $arr['fill'] = true;
            $arr['backgroundColor'] =  $this->colorsYear[$year];
            $arr['data'] = array_values($data_val);
            $dataSet[] = $arr;
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }


    public function ageRangeAtYear($results, $title){

        $results = $results->first();
        $attributes = array_keys(get_object_vars ( $results) );
        $vals = array_values(get_object_vars ( $results) );
        $labelX = $attributes;
        $labelYTxt = "เสียชีวิต";

        $dataSet = [];
        $colors =  array_slice($this->colorsIndex,0,count($labelX)); ;
            $arr = array();
            $arr['label'] = $labelYTxt;
            $arr['borderColor'] = $colors;
            $arr['fill'] = true;
            $arr['backgroundColor'] = $colors;
            $arr['data'] = $vals;
            $dataSet[] = $arr;


        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'pie';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;


    }


    public function ageRangeYear($title){


        $results = $this->deadCase_AgeRange_AtYear;
        $results = $results->first();
        $attributes = array_keys(get_object_vars ( $results) );

        $results = $this->deadCase_AgeRange_Year;
        $labelX = $attributes;
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            if($year < 2015 || $year > date('Y')){
                continue;
            }

            try{
                $data = (array) $results->where('year', '===', $year)->first();
                $data_val = [];
                foreach ($labelX as $x){
                    $data_val[$x] = 0;
                }
                unset($data['year']);
                $data_val = $data;

                $arr = array();
                $arr['label'] = $year;
                $arr['borderColor'] =  $this->colorsYear[$year];
                $arr['fill'] = true;
                $arr['backgroundColor'] =  $this->colorsYear[$year];
                $arr['data'] = array_values($data_val);
                $dataSet[] = $arr;
            }catch ( \Exception $e){
                dd($results->where('year', '===', $year));
            }
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }

    public function deathGenderYear($title){


        $results = $this->deadCase_Gender_Year;
        $results = $results->first();
        $attributes = array_keys(get_object_vars ( $results) );
        unset($attributes["2"]);

        $results = $this->deadCase_Gender_Year;
        $labelX = $attributes;
        $labelYTxt = $results->unique('year')->pluck('year');

        $dataSet = [];

        foreach ($labelYTxt as $year){

            if($year < 2015 || $year > date('Y')){
                continue;
            }

            try{
                $data = (array) $results->where('year', '===', $year)->first();
                $data_val = [];
                foreach ($labelX as $x){
                    $data_val[$x] = 0;
                }
                unset($data['year']);
                $data_val = $data;

                $arr = array();
                $arr['label'] = $year;
                $arr['borderColor'] =  $this->colorsYear[$year];
                $arr['fill'] = true;
                $arr['backgroundColor'] =  $this->colorsYear[$year];
                $arr['data'] = array_values($data_val);
                $dataSet[] = $arr;
            }catch ( \Exception $e){
                dd($results->where('year', '===', $year));
            }
        }

        $set = [];
        $set['title'] = $title;
        $set['graph'] = 'bar';
        $set['labelX'] = $labelX;
        $set['labelYTxt'] = $labelYTxt;
        $set['dataSet'] = $dataSet;
        $this->dataGraph['graphList'][] = $set;
    }


}
