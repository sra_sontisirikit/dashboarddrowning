<?php

namespace App\Http\Controllers;

use App\DeathCert;
use App\HDCdata;
use App\ISdata;
use App\ITEMSdata;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $isLiveRow = ISdata::liveRow();
        $isDeathRow = ISdata::deathRow();
        $deathRow = DeathCert::get();

        $hdc_person_accident_row = HDCdata::personAccidentRow();
        $hdc_person_death_row = HDCdata::personDeathRow();
        $hdc_person_ipd_row = HDCdata::personIPDRow();
        $hdc_person_opd_row = HDCdata::personOPDRow();

        $items_row = ITEMSdata::caseRow();


        $data = [];
        $data['death_row'] = count($deathRow);
        $data['is_live_row'] = $isLiveRow->count;
        $data['is_death_row'] = $isDeathRow->count;

        $data['hdc_death'] = $hdc_person_accident_row->count;
        $data['hdc_accident'] = $hdc_person_death_row->count;
        $data['hdc_ipd'] = $hdc_person_ipd_row->count;
        $data['hdc_opd'] = $hdc_person_opd_row->count;

        $data['items_row'] = $items_row->count;

        return view('home',$data);
    }

    public function deathcertIndex(Request $request){




        return view('deathcert.index');
    }

    public function hdcIndex(Request $request){
        return view('hdc.index');
    }

    public function isIndex(Request $request){
        return view('is.index');
    }
    public function itemsIndex(Request $request){
        return view('items.index');
    }

    public function satIndex(Request $request){
        return view('sat.index');
    }
}
