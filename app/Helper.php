<?php

function ranColor(){
    return "#".str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT);
}


function ranColors($size){
    $colors = [];
    for($i = 0 ; $i < $size ; $i++){
        $colors[] = ranColor();
    }
    return $colors;
}